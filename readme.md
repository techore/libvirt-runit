# libvirt-runit

## antiX Linux

This git repository contains the runit service files for libvirt, virtlock, and virtlog and are intended for use with antiX Linux.

## virt-manager

My primary use of libvirt is [virt-manager](https://virt-manager.org/). The runit service files are not specific to virt-manager and can be used for any application using libvirt.

## How to use

Download and unarchive or `git clone https://gitlab.com/techore/libvirt-runit` this repository, copy files, and link services.

```
cd /path/to/libvirt-runit

# Copy runit service files to /etc/sv/.
sudo cp -r etc/* /etc/

# Creates log directories at /var/log/runit/.
sudo cp -r var/* /var/

# Symlink service files to /etc/service/.
# antiX will automatically link to /etc/runit/runsvdir/current/.
ln -s /etc/sv/libvirtd /etc/service/libvirtd
ln -s /etc/sv/virtlockd /etc/service/virtlockd
ln -s /etc/sv/virtlogd /etc/service/virtlogd

# Start services
# You can safely ignore the warnings regarding missing dirs/files
# for they will be created.
sv start libvirtd
sv start virtlockd
sv start virtlogd

# Copy run files to /usr/share/runit/sv/.
# These may be overwritten by package updates where /etc/sv are not.
# There is no a antiX libvirt-runit package at this time, 2023-05-20.
mkdir /usr/share/runit/sv/libvirtd
mkdir /usr/share/runit/sv/virtlockd
mkdir /usr/share/runit/sv/virtlogd
cp /etc/sv/libvirtd/run /usr/share/runit/sv/libvirtd/
cp /etc/sv/virtlockd/run /usr/share/runit/sv/virtlockd/
cp /etc/sv/virtlockd/run /usr/share/runit/sv/virtlockd/
```

## Contributions

Please open "issues" if you identify a problem and do a PR or merge request to contribute.


Thank you,

techore
